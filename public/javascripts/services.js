angular.module('Dashboard.services', [])

  .factory('Devices', function($resource, $window) {
    return $resource($window.url + veedapiVersion + '/devices');
  })

  .factory('DateDiff', function($moment) {
    return {
      getType: function(startDate, endDate, callback) {
        var type = 'min';
        var diff = $moment.utc(endDate).diff($moment.utc(startDate), 'days', true);

        if (diff > 0.6 && diff <= 1.0) {
          type = 'hour';
        } else if (diff > 1.0 && diff <= 32.0) {
          type = 'day';
        } else if (diff > 28 && diff < 365) {
          type = 'month';
        } else if (diff > 365) {
          type = 'year';
        }

        callback(type);
      },
    };
  })

  .factory('QueryDevices', function(Devices, DeviceStats, $moment, _, DateDiff) {
    var query = function(startDate, endDate, callback) {
      var devs;

      var dataSeries = {};

      devs = Devices.query(function() {
        // remove promise and resolved from returned ngresource object
        delete devs.$promise;
        delete devs.$resolved;

        var timeInterval = 'min';

        DateDiff.getType(startDate, endDate, function(dateDiffType) {
          timeInterval = dateDiffType;
        });

        _.each(devs, function(device, idx) {
          DeviceStats.get({
            id: device.id,
            startTime: $moment.utc(startDate).format(),
            endTime: $moment.utc(endDate).format(),
            timeInterval: timeInterval
          }, function(deviceStats) {
            console.log(deviceStats);

            _.each(deviceStats.data, function(dataset, datasetIdx) {
              if (timeInterval === 'min') {
                // reformat into 5min intervals.
                var timeFormatHour = $moment(dataset.timestamp).format('HH');
                var timeFormatMinute = $moment(dataset.timestamp).format('mm');

                // round up to the next 5min interval
                var diff = timeFormatMinute % 5;
                timeFormatMinute = parseInt(timeFormatMinute) + 5 - diff;

                if (timeFormatMinute == 60) {
                  timeFormatHour = parseInt(timeFormatHour) + 1;
                  if (timeFormatHour >= 25) {
                    timeFormatHour = 24;
                  }
                  if (timeFormatHour < 10) {
                    timeFormatHour = '0' + timeFormatHour;
                  }
                  timeFormatMinute = '00'; // round-down if it's between 55 and 59
                }

                if (timeFormatMinute === 5) {
                  timeFormatMinute = '05';
                }

                var timeFormat = timeFormatHour + ':' + timeFormatMinute;
                if (dataSeries[timeFormat] === undefined) {
                  dataSeries[timeFormat] = {
                    activity: dataset.activity,
                    raw: dataset.raw,
                    heartrate: dataset.heartrate
                  }
                } else {
                  dataSeries[timeFormat].activity += dataset.activity;
                  dataSeries[timeFormat].raw += dataset.raw;
                  dataSeries[timeFormat].heartrate += dataset.heartrate;
                }
              } else if (timeInterval === 'hour') {
                var timeFormatHour = $moment(dataset.timestamp).format('HH');
                var timeFormat = timeFormatHour + ':00';
                if (dataSeries[timeFormat] === undefined) {
                  dataSeries[timeFormat] = {
                    activity: dataset.activity,
                    raw: dataset.raw,
                    heartrate: dataset.heartrate
                  }
                } else {
                  dataSeries[timeFormat].activity += dataset.activity;
                  dataSeries[timeFormat].raw += dataset.raw;
                  dataSeries[timeFormat].heartrate += dataset.heartrate;
                }
              } else if (timeInterval === 'day') {
                var timeFormat = $moment(dataset.timestamp).format('DD.MM.');

                dataSeries[timeFormat] = {
                  activity: dataset.activity,
                  raw: dataset.raw,
                  heartrate: dataset.heartrate
                }
              }


            });

            if (idx + 1 === devs.length) {
              callback(dataSeries);
            }

          });

        });

      });


    };

    return {
      query: query
    };

  })

  .factory('DeviceStats', function($resource, $window) {
    return $resource($window.url + veedapiVersion + '/device/:id/stats', {
      id: '@_id',
    }, {
      stats: {
        method: 'GET',
        isArray: true,
        params: {
          startTime: 'string',
          endTime: 'string',
          timeInterval: 'string',
        },
      },
    });
  })

  .factory('Device', function($resource, $window) {
    return $resource($window.url + veedapiVersion + '/device/:id', {
      id: '@_id',
    }, {
      update: {
        method: 'PUT',
      },
      delete: {
        method: 'DELETE',
      },
      participant: {
        method: 'GET',
        isArray: true,
        params: {
          participant: 'string',
        },
      },
    });
  })

  .factory('RawData', function($resource, $window) {
    return $resource($window.url + veedapiVersion + '/device/:id/rawData');
  })

  .factory('Export', function($resource, $window) {
    return $resource($window.url + veedapiVersion + '/devices/:id/:dataType/:dataSubtype/export', {
      id: '@id',
      dataType: '@dataType',
      dataSubtype: '@dataSubtype',
    }, {
      data: {
        method: 'GET',
        params: {
          fileType: 'string',
          startTime: 'string',
          endTime: 'string',
        },
      },
    });
  })

  .factory('Data', function($resource, $window) {
    return $resource($window.url + veedapiVersion + '/devices/:id/:dataType/:dataSubType', {
      id: '@id',
      dataType: '@dataType',
      dataSubType: '@dataSubType',
    }, {
      data: {
        method: 'GET',
        isArray: false,
        params: {
          startTime: 'string',
          endTime: 'string',
          interval: 'string',
        },
      },
    });
  })

