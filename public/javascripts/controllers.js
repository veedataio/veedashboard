'use strict';

angular.module('Dashboard.controllers', [
  'ui.router', 'ngResource', 'ngMaterial', 'ngAnimate', 'ngAria', 'ngMessages', 'chart.js',
  'mdPickers', 'angular-momentjs', 'underscore'])

  .controller('Heartrate__Controller', function($scope, Devices, Data, $moment, _) {


    $scope.isActive = function(deviceIdx, filter) {

      return filter === $scope.devices[deviceIdx].chart.active;
    };

    $scope.devices = $scope.devices = Devices.query(function() {
      // do nothing for now
      //console.log($scope.devices);

      var scale = {
        series: [],
        labels: [],
      };

      var numberOfValues = 1 * 60;
      for (var i = 0; i <= numberOfValues; i++) {
        scale.series.push();
        scale.labels.push($moment().subtract(1, 'hours').add(i, 'minutes').format('HH:mm'));

      }

      _.each($scope.devices, function(device, index) {
        device.chart = {
          active: 0,
          data: [
            scale.series,
          ],
          labels: scale.labels,
          series: ['HeartRate'],
          colors: ['#45356E', '#84AA00'],
          datasetOverride: [{yAxisID: 'y-axis-1'}],
          options: {
            responsive: true,
            maintainAspectRatio: false,

            scales: {
              xAxes: [{

                ticks: {
                  //max: 10,
                  autoSkip: true,
                  maxRotation: 0,
                  minRotation: 0
                }
              }],
              yAxes: [
                {
                  id: 'y-axis-1',
                  type: 'linear',
                  display: true,
                  position: 'left',
                  ticks: {
                    steps: 5,
                    stepValue: 5,
                    max: 250,
                    beginAtZero: true,

                  },
                }
              ]
            }
          }
        };

        $scope.updateHeartRate(index, 60, 'minutes', 0);

      });
    });


    $scope.updateHeartRate = function(deviceIndex, timeValue, timeInterval, active) {

      var deviceId = $scope.devices[deviceIndex].id;


      var scale = {
        series: [],
        labels: [],
      };

      var sum = 0;

      console.log('DeviceId: ' + deviceId + ' TimeValue: ' + timeValue + ' TimeInterval: ' + timeInterval);

      var labelFormat = 'HH:mm';
      var interval = 'min';

      if (timeInterval === 'hours') {
        interval = 'hour';
        labelFormat = 'HH:00';
      } else if (timeInterval === 'days') {
        interval = 'day';
        labelFormat = 'DD.MM';
      } else {
        interval = 'min';
      }

      for (var i = 0; i <= timeValue; i++) {
        var label = $moment().subtract(timeValue, timeInterval).add(i, timeInterval).format(labelFormat);

        scale.series.push(null);
        scale.labels.push(label);
      };

      Data.data({
        id: deviceId,
        dataType: 'sensor',
        dataSubType: 'heartRate',
        startTime: $moment.utc().subtract(timeValue, timeInterval).toJSON(),
        endTime: $moment.utc().toJSON(),
        interval: interval,
      }, function(sensordata) {
        _.each(sensordata.data, function(datapoint) {
          var time = $moment(datapoint.timestamp).format(labelFormat);
          var index = _.indexOf(scale.labels, time);
          sum += datapoint.value;
          scale.series[index] = Math.round(datapoint.value * 100) / 100;
        });
        $scope.devices[deviceIndex].chart.data[0] = scale.series;
        $scope.devices[deviceIndex].chart.labels = scale.labels;
        $scope.devices[deviceIndex].chart.active = active;
        $scope.devices[deviceIndex].chart.average = sensordata.calculated.average;
      });
    };
  })

  .controller('DeviceController', function($scope, Devices, QueryDevices, $moment, _, DateDiff) {
    $scope.active = 0;
    $scope.isActive = function(filter) {
      return filter === $scope.active;
    };

    $scope.type = 'min';
    $scope.startDate = $moment().subtract(6, 'hours').minute(0).toDate();
    $scope.endDate = $moment().toDate();

    $scope.devices = Devices.query(function() {
      QueryDevices.query($scope.startDate, $scope.endDate, function(data) {
        _.each($scope.devices, function(device) {

          if (device.lastSync && device.lastSync !== 0 && $moment(device.lastSync).isValid) {
            device.lastSync = $moment.utc(device.lastSync).local().format('dddd, MMMM Do YYYY, h:mm:ss a');

          } else {
            console.warn('Device ' + device.id + ' has an invalid lastSync value: ' + device.lastSync);
            device.lastSync = 'N/A';
          }
        });
        $scope.updateChart(data);
      });
    });


    $scope.updateData = function() {
      $scope.devices = Devices.query(function() {
        QueryDevices.query($scope.startDate, $scope.endDate, function(data) {
          _.each($scope.devices, function(device) {


            if (device.lastSync && device.lastSync !== 0 && $moment(device.lastSync).isValid) {
              device.lastSync = $moment.utc(device.lastSync).local().format('dddd, MMMM Do YYYY, h:mm:ss a');

            } else {
              console.warn('Device ' + device.id + ' has an invalid lastSync value: ' + device.lastSync);
              device.lastSync = 'N/A';
            }
          });
          $scope.updateChart(data);
        });
      });
    };

    $scope.datasetOverride = [
      {
        yAxisID: 'TrainingRaw',
      }, {
        yAxisID: 'TrainingActivity',
      },
    ];

    $scope.series = ['TrainingRaw', 'TrainingActivity'];
    $scope.colors = ['#45356E', '#84AA00'];

    $scope.options = {
      legend: {
        display: true,
      },
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [
          {
            stacked: true,
            ticks: {
              autoSkip: true,
              maxRotation: 0,
              minRotation: 0
            }
          },
        ],
        yAxes: [
          {
            id: 'TrainingRaw',
            type: 'linear',
            display: true,
            stacked: true,
            ticks: {
              beginAtZero: true,
            },
          },
          {
            id: 'TrainingActivity',
            type: 'linear',
            display: true,
            stacked: true,
            ticks: {
              beginAtZero: true,

            },
          },
        ],
      },
    };

    $scope.clickHours = function() {
      $scope.startDate = $moment().subtract(6, 'hours').minute(0).toDate();
      $scope.endDate = $moment().toDate();
      $scope.active = 0;
      $scope.updateData();
    };

    $scope.clickMonth = function() {
      $scope.startDate = $moment().subtract(30, 'days').toDate();
      $scope.endDate = $moment().endOf('day').toDate();
      $scope.active = 2;
      $scope.updateData();
    };

    $scope.clickWeek = function() {
      $scope.startDate = $moment().subtract(7, 'days').toDate();
      $scope.endDate = $moment().endOf('day').toDate();
      $scope.active = 1;
      $scope.updateData();
    };

    $scope.updateChart = function(data) {
      var timeseriesRaw = {};
      var timeseriesActivity = {};
      var label = [];

      $scope.type = 'min';
      var diff = $moment($scope.endDate).diff($moment($scope.startDate), 'days', true);
      var i;
      var timestamp;
      DateDiff.getType($scope.startDate, $scope.endDate, function(dateDiffType) {
        $scope.type = dateDiffType;
      });

      if ($scope.type === 'day') {
        for (i = 0; i <= diff; i++) {
          timestamp = $moment($scope.startDate).add(i, 'days').format('DD.MM.');

          label[timestamp] = timestamp;
          if (data[timestamp]) {
            timeseriesActivity[timestamp] = data[timestamp].activity;
            timeseriesRaw[timestamp] = data[timestamp].raw;
          } else {
            timeseriesActivity[timestamp] = 0;
            timeseriesRaw[timestamp] = 0;
          }

        }
      } else if ($scope.type === 'hour') {
        diff = $moment($scope.endDate).diff($moment($scope.startDate), 'hours', true);
        for (i = 0; i <= diff; i++) {
          timestamp = $moment($scope.startDate).add(i, 'hours').format('HH:mm');

          if (data[timestamp]) {
            timeseriesActivity[timestamp] = data[timestamp].activity;
            timeseriesRaw[timestamp] = data[timestamp].raw;
          } else {
            timeseriesActivity[timestamp] = 0;
            timeseriesRaw[timestamp] = 0;
          }

        }
      } else if ($scope.type === 'min') {
        // add 5min safety
        diff = ($moment($scope.endDate).diff($moment($scope.startDate), 'minutes', true) / 5) + 5;

        for (i = 0; i <= diff; i++) {
          var timestampMoment = $moment($scope.startDate).add(i * 5, 'minutes');
          timestamp = timestampMoment.format('HH:mm');

          if (timestampMoment.format('mm') === '00' || timestampMoment.format('mm') === '30') {
            label[timestamp] = timestamp;
          } else {
            label[timestamp] = '';
          }
          if (data[timestamp]) {
            timeseriesActivity[timestamp] = data[timestamp].activity;
            timeseriesRaw[timestamp] = data[timestamp].raw;
          } else {
            timeseriesActivity[timestamp] = null;
            timeseriesRaw[timestamp] = null;
          }


        }
      }


      $scope.labels = _.values(label);

      $scope.data = [
        _.values(timeseriesRaw), // Raw
        _.values(timeseriesActivity), // Activity

      ];

    };


  })


  .controller('ExportController', function($scope, Devices, DeviceStats, $moment, _, Export,
                                           QueryDevices, $window) {

    $scope.devices = Devices.query(function() {
      _.each($scope.devices, function(device, i) {
        device.isSelected = true;
        if (device.lastSync && device.lastSync !== 0 && $moment(device.lastSync).isValid) {
          device.lastSync = $moment.utc(device.lastSync).local().format('dddd, MMMM Do YYYY, h:mm:ss a');

        } else {
          console.warn('Device ' + device.id + ' has an invalid lastSync value: ' + device.lastSync);
          device.lastSync = 'N/A';
        }
      });
    });

    $scope.exportStarted = false;
    $scope.export = {
      trainingRaw: {
        url: '',
      },
      trainingActivity: {
        url: '',
      },
    };

    $scope.selected = [];
    $scope.typeActivity = true;
    $scope.typeRaw = true;

    $scope.isAllChecked = function() {
      //return $scope.selected.length === $scope.devices.length;
    };

    $scope.isIndeterminate = function() {
      return ($scope.selected.length !== 0 &&
      $scope.selected.length !== $scope.devices.length);
    };

    $scope.toggleAll = function() {
      if ($scope.selected.length === $scope.devices.length) {
        $scope.selected = [];
      } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
        $scope.selected = [];
        _.each($scope.devices, function(item) {
          $scope.selected.push(item.id);
        });
      }
    };

    // TODO: look into why this is being called multiple times,
    // as of currently no issue with it, but might be later
    $scope.exists = function(item, list) {
      return list.indexOf(item.id) > -1;
    };

    $scope.toggle = function(item, list) {
      var idx = list.indexOf(item.id);
      if (idx > -1) {
        list.splice(idx, 1);
      } else {
        list.push(item.id);
      }
    };


    $scope.exportData = function() {
      $scope.exportStarted = true;
      $scope.exported = [];

      // TODO: more generic and sophisticated, but works for now
      var selectedType = [];
      if ($scope.typeActivity) {
        selectedType.push('activity');
      }

      if ($scope.typeRaw) {
        selectedType.push('raw');
      }

      if ($scope.heartrate) {
        selectedType.push('heartRate');
      }

      _.each($scope.selected, function(deviceId) {
        _.each(selectedType, function(subType) {
          // temporarly workaround to accomodate datatype/dataSubtype
          var dataType = 'training';

          if (subType === 'heartRate') {
            dataType = 'sensor';
          }


          Export.data({
            id: deviceId,
            dataType: dataType,
            dataSubtype: subType,
            startTime: $moment.utc($scope.startDate).format(),
            endTime: $moment.utc($scope.endDate).format(),
            fileType: 'json',
          }, function(exportInfo) {

            $scope.exported.push({
              filename: dataType + '_' + subType + '.json',
              url: $window.url + '/' + exportInfo.url
            });
          });
        })
      });

      console.log($scope.exported);

      $scope.exportStarted = false;


    };

    $scope.filterAllDevices = true;

    $scope.startDate = $moment().startOf('day').toDate();
    $scope.endDate = $moment().endOf('day').toDate();


    $scope.type = 'min';


  });