'use strict';
var log = require('../config/log')(module);

var records = [
  {
    id: 1,
    username: 'alex',
    password: 'alex',
    displayName: 'Alex',

  },
  {
    id: 2,
    username: 'pablo',
    password: 'pablo',
    displayName: 'Pablo',

  },
  {
    id: 3,
    username: 'igor',
    password: 'veem0t!on',
    displayName: 'Igor',

  },
  {
    id: 4,
    username: 'kayla',
    password: 'getmoving',
    displayName: 'Kayla',

  },
];

exports.findById = function(id, cb) {
  log.info(id);
  process.nextTick(function() {
    var idx = id - 1;
    if (records[idx]) {

      cb(null, records[idx]);
    } else {
      cb(new Error('User ' + id + ' does not exist'));
    }
  });
};

exports.findByUsername = function(username, cb) {

  process.nextTick(function() {
    for (var i in records) {
      if (records[i].username === username.username) {

        return cb(null, records[i]);
      }
    }

    return cb(null, null);
  });
};
