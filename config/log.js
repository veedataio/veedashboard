'use strict';

var winston = require('winston');
var moment = require('moment');
winston.setLevels(winston.config.npm.levels);
winston.addColors(winston.config.npm.colors);
var util = require('util');

var path = require('path');

var getLabel = function(module) {
  var parts = module.filename.split('/');
  return parts[parts.length - 2] + '/' + parts.pop();
};

var level = 'debug';
if (process.env.NODE_ENV == 'test') {
  level = 'emerg';
}

module.exports = function(module) {
  return new (winston.Logger)({
    transports: [
      /* uncomment to enable file-logging again
       new winston.transports.File({
       filename: path.join(__dirname, '..', 'log', 'winston.log'),
       level: 'debug',
       colorize: true,
       timestamp: function() {
       return moment().format('DD.MM.YYYY HH:mm');
       },
       formatter: function(options) {
       // Annotation in ' is optional, example:
       // INFO: [23.08.2016 10:27 '- CODEPATH'] LOGMESSAGE '{META}'
       var format = winston.config.colorize(options.level, options.level.toUpperCase()) +
       ': [' + this.timestamp() + (getLabel(module) === undefined ? '' : ' - ' +
       getLabel(module)) + '] ' + options.message + ' ' +
       (Object.keys(options.meta).length === 0 ? '' : JSON.stringify(options.meta));

       return format;
       },

       }),
       */

      new (winston.transports.Console)({
        level: level,
        colorize: true,
        timestamp: function() {
          return moment().format('DD.MM.YYYY HH:mm');
        },
        formatter: function(options) {
          // Annotation in ' is optional, example:
          // INFO: [23.08.2016 10:27 '- CODEPATH'] LOGMESSAGE '{META}'

          var level = winston.config.colorize(options.level, options.level.toUpperCase());
          var timestamp = ': [' + this.timestamp();
          var codepath = (getLabel(module) === undefined ? '' : ' - ' + getLabel(module)) + '] ';
          var logmessage = options.message + ' ';
          var meta = (Object.keys(options.meta).length === 0 ? '' : util.inspect(options.meta));

          return level + timestamp + codepath + logmessage + meta;
        },
      }),
    ],
  })
    ;
};

