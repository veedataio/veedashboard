'use strict';

//noinspection NodeModulesDependencies
var veedapiVersion = '/0.0.3';

angular.module('Dashboard', ['chart.js', 'Dashboard.services', 'Dashboard.controllers'])

  .config(function(ChartJsProvider) {
    ChartJsProvider.setOptions({
      colors: ['#ff0000', '#00ff00', '#0000ff', '#f0f0f0', '#0f0f0f', '#fff000', '#000fff'],
    });
  })

  .config(function($stateProvider) {
    $stateProvider
      .state('devices', {
        url: '/devices',
        templateUrl: 'templates/devices',
        controller: 'DeviceController',
      })
      .state('heartrate', {
        url: '/heartrate',
        templateUrl: 'templates/heartrate',
        controller: 'Heartrate__Controller',
      })
      .state('export', {
        url: '/export',
        templateUrl: 'templates/export',
        controller: 'ExportController',
      });
  })

  .run(function($state) {
    $state.go('devices');
  });
