'use strict';
var log = require('./config/log')(module);
var level = 'info';
if (process.env.NODE_ENV == 'test') {
  level = 'emerg'; // hide logs for tests
}

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var lessMiddleware = require('less-middleware');

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
var db = require('./passport');

var winston = require('winston');
var expressWinston = require('express-winston');


var util = require('util');

passport.use(new LocalStrategy(
  function(username, password, done) {
    db.users.findByUsername({
      username: username,
    }, function(err, user) {
      if (err) {
        return done(err);
      }

      if (!user) {
        return done(null, false, {
          message: 'Incorrect username.' + username,
        });
      }

      if (user.password !== password) {
        return done(null, false, {
          message: 'Incorrect password.',
        });
      }

      return done(null, user);
    });
  }
));

passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  db.users.findById(id, function(err, user) {
    if (err) {
      return cb(err);
    }

    cb(null, user);
  });
});

var app = express();

app.use(expressWinston.logger({
    transports: [
      new winston.transports.Console({
        json: false,
        colorize: true,
        level: level,
      }),
    ],
    msg: 'HTTP {{req.method}} {{req.url}}',
    expressFormat: true,
    colorize: true,
  }
));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(require('morgan')('combined'));

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false,
}));
app.use(cookieParser());

app.use(require('express-session')({
  secret: 'vdtsecr3t',
  resave: false,
  saveUninitialized: false,
}));
app.use(lessMiddleware(path.join(__dirname, 'less'), {
  preprocess: {
    path: function(pathname, req) {
      return pathname.replace(/\/css\//, '/');
    },
  },
  dest: path.join(__dirname, 'public'),
  yuicompress: true,
}));

app.use(express.static(path.join(__dirname, 'public')));
app.use('/bc', express.static(path.join(__dirname, '/bower_components')));
app.use(passport.initialize());
app.use(passport.session());

app.get('/', function(req, res) {
  res.render('index', {
    title: 'test',
  });
});

app.get('/templates/:template', function(req, res) {
  var template = req.params.template;
  res.render('templates/' + template);
});

app.get('/js/Chart.min.js', function(req, res) {
  res.sendFile(path.join(__dirname, 'node_modules', 'chart.js', 'dist') + '/Chart.min.js');
});

app.get('/js/angular-chart.min.js', function(req, res) {
  res.sendFile(path.join(__dirname, 'node_modules', 'angular-chart.js', 'dist') + '/angular-chart.min.js');
});

app.get('/login', function(req, res) {
  res.render('login');
});

app.post('/login', passport.authenticate('local', {
  successReturnToOrRedirect: '/',
  failureRedirect: '/login',
}), function(req, res) {
  res.redirect('/');
});

app.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/login');
});

app.get('/dashboard', ensureLoggedIn, function(req, res) {
  res.render('dashboard', {
    title: 'dashboard',
    user: req.user,
    url: process.env.API_URL,
  });
});

app.get('/demo', ensureLoggedIn, function(req, res) {
  res.render('presentation/index', {
    title: 'demo',
    user: req.user.username,
  });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err,
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {},
  });
});

module.exports = app;

app.listen(8081, function() {
  log.info('Veedashboard server started. [Veedapi url = %s ]', process.env.API_URL);
});
